from sqlalchemy.sql import func

from project import db


class Url(db.Model):

    __tablename__ = "urls"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    url = db.Column(db.String(2000), nullable=False, index=True, unique=True)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, url):
        self.url = url
