import re
import string
from flask import flash, Blueprint, jsonify, request, render_template, redirect
from sqlalchemy import exc

from project import db
from project.api.models import Url

shortener_blueprint = Blueprint("shortener", __name__, template_folder="./templates")


def id_to_shortURL(num, base=62):
    """ Generates a short url from integer"""
    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    short_url = ""
    while num:
        short_url = chars[num % base] + short_url
        num = num // 62
    return short_url


def shortURL_to_id(short_url, base=62):
    """ Reverse a short url back to integer"""
    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    res = 0
    for c in short_url:
        res = res * base + chars.find(c)
    return res


@shortener_blueprint.route("/", methods=["GET", "POST"])
def index():
    """ Creates shortened URL from provided url in form."""
    if request.method == "POST":
        # url is validated in form
        url = request.form["url"]
        # TODO LRU Cache
        try:
            url_record = db.session.query(Url).filter_by(url=url).first()
            if not url_record:
                # add if not in database
                url_record = Url(url=url)
                db.session.add(url_record)
                db.session.commit()
            short_url = id_to_shortURL(url_record.id)
            return render_template("index.html", short_url=request.url_root + short_url)
        except exc.IntegrityError:
            db.session.rollback()
            flash("Internal error")
    return render_template("index.html")


@shortener_blueprint.route("/<short_url>")
def redirect_short_url(short_url):
    """ Redirects shortened url to original one."""
    # TODO LRU Cache
    try:
        record_id = shortURL_to_id(short_url)
        url = Url.query.filter_by(id=int(record_id)).first()
        if not url:
            flash("Shortened URL does not exist!")
            return render_template("index.html")
        else:
            return redirect(url.url)
    except ValueError:
        flash("Internal error")
        return render_template("index.html")


def validate_url(url):
    regex = re.compile(
        r"^(?:http|ftp)s?://"  # http:// or https://
        # domain
        r"(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|"  # noqa
        r"localhost|"  # localhost...
        r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})"  # ...or ip
        r"(?::\d+)?"  # optional port
        r"(?:/?|[/?]\S+)$",
        re.IGNORECASE,
    )
    return re.match(regex, url) is not None


@shortener_blueprint.route("/urls", methods=["POST"])
def add_url():
    post_data = request.get_json()
    response_object = {"status": "fail", "message": "Invalid payload."}
    if not post_data or not post_data.get("url"):
        # invalid json
        return jsonify(response_object), 400
    url = post_data.get("url")
    if not validate_url(url):
        response_object["message"] = "Provided URL not valid."
        return jsonify(response_object), 400
    try:
        url_record = db.session.query(Url).filter_by(url=url).first()
        if not url_record:
            # add if not in database
            url_record = Url(url=url)
            db.session.add(url_record)
            db.session.commit()
        short_url = id_to_shortURL(url_record.id)
        response_object["status"] = "success"
        response_object["message"] = f"{url} was shortened."
        response_object["data"] = {"short_url": request.url_root + short_url}
        return jsonify(response_object), 201
    except exc.IntegrityError:
        db.session.rollback()
        return jsonify(response_object), 400


@shortener_blueprint.route("/urls/<short_url>", methods=["GET"])
def get_url(short_url):
    """ Get original url for shortened one."""
    response_object = {"status": "fail", "message": "Shortened URL does not exist."}
    # TODO think about LRU cache
    try:
        record_id = shortURL_to_id(short_url)
        url = Url.query.filter_by(id=int(record_id)).first()
        if not url:
            return jsonify(response_object), 404
        else:
            response_object = {"status": "success", "data": {"url": url.url}}
            return jsonify(response_object), 200
    except ValueError:
        return jsonify(response_object), 404


@shortener_blueprint.route("/urls", methods=["GET"])
def get_all_urls():
    """ Get all urls stored in db """
    response_object = {
        "status": "success",
        "data": {"urls": [url.url for url in Url.query.all()]},
    }
    return jsonify(response_object), 200
