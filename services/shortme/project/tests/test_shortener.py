import json
import unittest

from project import db
from project.api.shortener import id_to_shortURL
from project.api.models import Url
from project.tests.base import BaseTestCase


class TestShortmeService(BaseTestCase):
    """Tests for the Shortme Service."""

    def test_add_url(self):
        """ Ensure a new url can be added to the database."""
        with self.client:
            response = self.client.post(
                "/urls",
                data=json.dumps({"url": "http://www.sportnet.hr"}),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())

            self.assertEqual(response.status_code, 201)
            self.assertIn("http://www.sportnet.hr was shortened.", data["message"])
            self.assertIn("success", data["status"])

    def test_add_url_invalid_json(self):
        """ Ensure error is thrown if the JSON object is empty."""
        with self.client:
            response = self.client.post(
                "/urls", data=json.dumps({}), content_type="application/json"
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("Invalid payload.", data["message"])
            self.assertIn("fail", data["status"])

    def test_add_url_invalid_json_keys(self):
        """ Ensure error is thrown if the JSON object does not have a url key."""
        with self.client:
            response = self.client.post(
                "/urls",
                data=json.dumps({"test": "http://www.example.org"}),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("Invalid payload.", data["message"])
            self.assertIn("fail", data["status"])

    def test_add_url_invalid_url(self):
        """ Ensure error is thrown if the url is invalid."""
        with self.client:
            response = self.client.post(
                "/urls",
                data=json.dumps({"url": "invalid"}),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 400)
            self.assertIn("Provided URL not valid.", data["message"])
            self.assertIn("fail", data["status"])

    def test_add_existing_url(self):
        """ Ensure existing url returns correct short url."""
        with self.client:
            response = self.client.post(
                "/urls",
                data=json.dumps({"url": "http://www.index.hr"}),
                content_type="application/json",
            )
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 201)
            self.assertIn("http://www.index.hr was shortened.", data["message"])
            self.assertIn("success", data["status"])
            self.assertIn("b", data["data"]["short_url"])

    def test_get_url(self):
        """ Ensure get url behaves correctly."""
        url = Url(url="http://www.coursera.org")
        db.session.add(url)
        db.session.commit()
        with self.client:
            response = self.client.get("urls/" + id_to_shortURL(url.id))
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 200)
            self.assertEqual(data["data"]["url"], url.url)
            self.assertIn("success", data["status"])

    def test_get_url_nonexisting(self):
        with self.client:
            response = self.client.get("urls/0-2313")
            data = json.loads(response.data.decode())
            self.assertEqual(response.status_code, 404)
            self.assertIn("fail", data["status"])
            self.assertIn("Shortened URL does not exist.", data["message"])


if __name__ == "__main__":
    unittest.main()
