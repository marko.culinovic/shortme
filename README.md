# ShortMe

ShortMe is a URL shortener built using nginx, gunicorn, Flask, postgresql and docker.
In production app relies on gunicorns asynchronous workers(via Gevent).
Application is deployed at: http://52.205.178.197/



## Endpoints

| Endpoint        | HTTP Method           | CRUD Method  | Result |
| --------------- |-----------------------| -------------|--------|
| /     | GET | READ | ShortMe web application|
| /urls      | GET      |   READ | Get all original urls|
| /urls/<short_url> | GET      |    READ | Get original url from shortened form|
| /urls | POST | CREATE | Add url and create shortened|

## Requirements

Ensure that you have Docker and Docker Compose installed.

Build the image from the project root:
`docker-compose -f docker-compose-dev-yml build`

Fire up the container:
`docker-compose -f docker-compose-dev-yml up -d`